package net.scero.service;

/*
 * Process messages form SQS Queue
 *
 * @author joseluis.nogueira on 08/02/2019
 */
public class SQSProcessMessage {
  public void process(String message){
    System.out.println("Process message: " + message);
  }
}
