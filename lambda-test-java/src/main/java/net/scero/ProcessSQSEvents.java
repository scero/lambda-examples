package net.scero;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.SQSEvent;
import com.amazonaws.services.lambda.runtime.events.SQSEvent.SQSMessage;
import net.scero.service.SQSProcessMessage;

/*
 * Handler for event SQSEvent
 *
 * @author joseluis.nogueira on 07/02/2019
 */
public class ProcessSQSEvents implements RequestHandler<SQSEvent, Void> {
  private static final SQSProcessMessage sqsProcessMessage;
  static {
    sqsProcessMessage = new SQSProcessMessage();
  }
  @Override
  public Void handleRequest(SQSEvent event, Context context)
  {
    for(SQSMessage msg : event.getRecords()){
      sqsProcessMessage.process(msg.getBody());
    }
    return null;
  }
}
