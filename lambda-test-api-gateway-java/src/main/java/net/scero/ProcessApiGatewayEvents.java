package net.scero;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyRequestEvent ;
import main.java.net.scero.service.ApiGatewayProcessMessage;

/*
 * Handler for event from API Gateway
 *
 * @author joseluis.nogueira on 07/02/2019
 */
public class ProcessApiGatewayEvents implements RequestHandler<APIGatewayProxyRequestEvent, Void> {
  private static final ApiGatewayProcessMessage apiGatewayProcessMessage;
  static {
    apiGatewayProcessMessage = new ApiGatewayProcessMessage();
  }
  @Override
  public Void handleRequest(APIGatewayProxyRequestEvent event, Context context)
  {
      if (event.getHttpMethod().equals("GET")){
        apiGatewayProcessMessage.process(event.getQueryStringParameters().entrySet());
      }
    return null;
  }
}
