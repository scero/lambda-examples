package net.scero.service;

import java.util.Map;
import java.util.Set;

/*
 * Process messages form API Gateway
 *
 * @author joseluis.nogueira on 08/02/2019
 */
public class ApiGatewayProcessMessage {
  public void process(Set<Map.Entry<String, String>> parameters){
    StringBuilder sb = new StringBuilder().append("Process message with parameters: ");
    for(Map.Entry<String, String> entry : parameters){
      sb.append(entry.getKey()).append(" = ").append(entry.getValue());
    }
    System.out.println(sb.toString());
  }
}
