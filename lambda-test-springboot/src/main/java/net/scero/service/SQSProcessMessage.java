package net.scero.service;

import org.springframework.stereotype.Service;

/*
 *
 *
 * @author joseluis.nogueira on 08/02/2019
 */
@Service
public class SQSProcessMessage {
  public void process(String message){
    System.out.println("Process message: " + message);
  }
}
