package net.scero;

import com.amazonaws.serverless.exceptions.ContainerInitializationException;
import com.amazonaws.serverless.proxy.model.AwsProxyRequest;
import com.amazonaws.serverless.proxy.model.AwsProxyResponse;
import com.amazonaws.serverless.proxy.spring.SpringBootLambdaContainerHandler;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.SQSEvent;
import com.amazonaws.services.lambda.runtime.events.SQSEvent.SQSMessage;
import net.scero.service.SQSProcessMessage;
import org.springframework.stereotype.Component;

/*
 *
 *
 * @author joseluis.nogueira on 07/02/2019
 */
@Component
public class ProcessSQSEvents implements RequestHandler<SQSEvent, Void> {
  private static SpringBootLambdaContainerHandler<AwsProxyRequest, AwsProxyResponse> handler;
  static {
    try {
      handler = SpringBootLambdaContainerHandler.getAwsProxyHandler(Application.class);
    } catch (ContainerInitializationException e) {
      // if we fail here. We re-throw the exception to force another cold start
      e.printStackTrace();
      throw new RuntimeException("Could not initialize Spring Boot application", e);
    }
  }

  @Override
  public Void handleRequest(SQSEvent event, Context context)
  {
    SQSProcessMessage sqsProcessMessage = Application.getApplicationContext().getBean(SQSProcessMessage.class);
    for(SQSMessage msg : event.getRecords()){
      sqsProcessMessage.process(msg.getBody());
    }
    return null;
  }
}
