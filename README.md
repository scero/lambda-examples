**lambda-test-java**: Ejemplo del manejo de un evento generado por una cosa SQS, con Java puro

**lambda-test-springboot**: Ejemplo del manejo de un evento generado por una cosa SQS, con SpringBoot

**lambda-test-api-gateway-java**: Ejemplo del manejo de un evento generado por un API Gateway, con Java puro

**lambda-test-api**: Ejemplo del manejo de un api, con SpringBoot. Este api debería ser atacado por un API Gateway para que fuese visible