package net.scero.controller;


import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;


@RestController
@EnableWebMvc
public class PingController {
    @RequestMapping(path = "/ping", method = RequestMethod.GET)
    public Map<String, String> ping() throws IOException {
        System.out.println("Ping execution");

        Map<String, String> pong = new HashMap<>();
        pong.put("pong", "Hello, World!");

        return pong;
    }
}
